// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab4GameModeBase.h"
#include "Lab4Character.h"
#include "Lab4PlayerController.h"

ALab4GameModeBase::ALab4GameModeBase() {
    PlayerControllerClass = ALab4PlayerController::StaticClass();

    // set defualt pawn class to out character class
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprint/MyLab4CharacterBP.MyLab4CharacterBP_C'"));

    if(pawnBPClass.Object) {
        UClass * pawnBP = (UClass*)pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    } else {
        DefaultPawnClass = ALab4Character::StaticClass();
    }
    
}